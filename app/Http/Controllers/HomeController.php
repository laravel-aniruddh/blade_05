<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function name(){
        $name="aniruddh";
        return view('name',['name'=>$name]);
    }

    public function names(){
        $name="aniruddh";
        return view('names',['name'=>$name]);
    }

    public function elseif(){
        $name="nikunj";
        return view('ifelseif',['name'=>$name]);
    }

    public function unless(){
        $name="aniruddh";
        return view('unless',['name'=>$name]);
    }

    public function switch(){
        return view('switch');
    }

    public function forloop(){
        return view('forloop');
    }

    public function foreach(){
        $bhagvan=['Krishna','Mahadev','Maa'];
        return view('foreach',['bhagvan'=>$bhagvan]);
    }

    public function forelse(){
        $bhagvan=[];
        return view('forelse',['bhagvan'=>$bhagvan]);
    }
}
