<?php

use Illuminate\Support\Facades\Route;
use\App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home',[HomeController::class,'name']); //@if

Route::get('name',[HomeController::class,'names']); //@if eles

Route::get('elseif',[HomeController::class,'elseif']); //@if else if

Route::get('unless',[HomeController::class,'unless']); //@unless

Route::get('switch',[HomeController::class,'switch']); //@switch

Route::get('forloop',[HomeController::class,'forloop']); //@forloop

Route::get('foreach',[HomeController::class,'foreach']); //@foreach

Route::get('forelse',[HomeController::class,'forelse']); //@forelse

